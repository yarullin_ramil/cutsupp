package com.itprom.project.suppcut;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuppcutApplication {

	public static void main(String[] args) {
		SpringApplication.run(SuppcutApplication.class, args);
	}

}
